package alien.taskQueue.jobsplit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alien.catalogue.LFN;
import alien.catalogue.LFNUtils;
import alien.catalogue.PFN;
import alien.se.SE;
import alien.se.SEUtils;
import alien.taskQueue.JDL;
import alien.taskQueue.TaskQueueUtils;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public class SplitAF extends JobSplitter {

	/**
	 * queue id for masterjob
	 */
	private long masterId;

	/**
	 * list of subjobs to submit
	 */
	private final List<JDL> subjobs = new ArrayList<>();

	/**
	 * @param strategy
	 */
	SplitAF(final String strategy) {
		super.strategy = strategy;
	}

	Collection<LFN> groupInputFiles(final JDL mJDL) throws IOException, IllegalArgumentException {

		final Collection<LFN> inputFiles = mJDL.getInputLFNs();

		checkNoDownload();

		if (inputFiles.isEmpty())
			throw new IOException("Could not find inputdata files!");

		if (masterJdl.getb("ForceOnlySEInput", false)) {

			final List<Integer> forcedSE = new ArrayList<>();
			final List<LFN> matchingLFN = new ArrayList<>();

			if (masterJdl.gets("Requirements") != null) {
				final String reqPat = "member\\(other.CloseSE,\"(.*?)\"\\)";
				final Pattern p = Pattern.compile(reqPat);
				final Matcher m = p.matcher(masterJdl.gets("Requirements"));

				while (m.find()) {
					final String se = m.group(1);
					SE tmpSE = null;
					if (!se.equals(""))
						tmpSE = SEUtils.getSE(m.group(1));
					if (tmpSE == null)
						throw new IOException("Did not find SE that is forced, " + se);

					forcedSE.add(Integer.valueOf(tmpSE.seNumber));
				}
			}

			final Map<LFN, Set<PFN>> lfnToPfn = LFNUtils.getRealPFNs(inputFiles);

			if (lfnToPfn.isEmpty())
				throw new IOException("Did not get any PFNs from LFNs");

			final List<LFN> missingPFN = new ArrayList<>();
			final List<LFN> PFNEmpty = new ArrayList<>();
			final List<LFN> ignoredLFN = new ArrayList<>();

			for (final LFN lfn : inputFiles) {
				if (!lfnToPfn.containsKey(lfn))
					missingPFN.add(lfn);

				else if (lfnToPfn.get(lfn).isEmpty()) {
					PFNEmpty.add(lfn);
				}

				else {

					for (final PFN pfn : lfnToPfn.get(lfn)) {
						if (forcedSE.contains(Integer.valueOf(pfn.seNumber)))
							matchingLFN.add(lfn);
						else
							ignoredLFN.add(lfn);
					}

				}

			}
			int threshold = 10;
			if (masterJdl.getInteger("MaxInputMissingThreshold") != null) {
				threshold = masterJdl.getInteger("MaxInputMissingThreshold").intValue();
			}

			if (!missingPFN.isEmpty())
				TaskQueueUtils.putJobLog(masterId, "trace", "LFN's failed to retrieve because missing PFN numbers: " + missingPFN.size(), null);
			if (!PFNEmpty.isEmpty())
				TaskQueueUtils.putJobLog(masterId, "trace", "LFN's failed to retrieve because PFN is empty numbers: " + PFNEmpty.size(), null);
			if (!ignoredLFN.isEmpty())
				TaskQueueUtils.putJobLog(masterId, "trace", "LFN's ignored because it does exist on given SE numbers: " + ignoredLFN.size(), null);
			if (matchingLFN.size() <= inputFiles.size() * ((float) (100 - threshold) / 100)) {
				throw new IOException("LFN's matching SE found under threshold");
			}

			return matchingLFN;
		}

		for (final LFN lfn : inputFiles) {
			lfn.lfn = getFinalLfnString(lfn.getCanonicalName());
		}
		return inputFiles;
	}

	@Override
	List<JDL> splitJobs(final JDL mJDL, final long lMasterId) throws IOException, IllegalArgumentException {

		masterJdl = mJDL;
		this.masterId = lMasterId;
		final Collection<LFN> inputFiles = groupInputFiles(mJDL);
		List<String> tempInput = new ArrayList<>();

		long maxinputsize = 0;
		int maxinputnumber = 0;

		if (mJDL.gets("SplitMaxInputFileNumber") != null) {
			maxinputnumber = Integer.parseInt(mJDL.gets("SplitMaxInputFileNumber"));
		}

		else if (mJDL.gets("SplitMaxInputFileSize") != null) {
			maxinputsize = mJDL.getSize("SplitMaxInputFileSize", 'G', 1);
		}
		else
			throw new IllegalArgumentException("Split by AF is required to set either max number or max size of input files per subjob");

		long currentSize = 0;

		final JDL baseJDL = prepareSubJobJDL(mJDL, lMasterId);

		if (mJDL.gets("OrderLFN") != null) {
			sortStrategy = mJDL.gets("SplitMaxInputFileNumber");
		}

		tempInput = new ArrayList<>();

		if (maxinputsize > 0) {
			for (final LFN lfn : inputFiles) {

				if (maxinputsize < currentSize + lfn.getSize() && !tempInput.isEmpty()) {
					addSubjob(baseJDL, tempInput, currentSize);
					tempInput = new ArrayList<>();
					currentSize = 0;
				}

				currentSize += lfn.getSize();
				tempInput.add(lfn.lfn);

			}

		}

		if (maxinputnumber != 0) {

			int currentnumber = 0;
			int numberGroups = (int) Math.ceil((double) inputFiles.size() / maxinputnumber);
			if (numberGroups == 0)
				numberGroups = 1;

			int tmpMax = inputFiles.size() / numberGroups;

			for (final LFN lfn : inputFiles) {

				if (currentnumber >= tmpMax && currentnumber != 0) {
					numberGroups--;
					tmpMax = ((inputFiles.size() - currentnumber) / numberGroups) + currentnumber;
					addSubjob(baseJDL, tempInput, currentSize);
					tempInput = new ArrayList<>();

				}
				tempInput.add(lfn.lfn);
				currentnumber++;
			}
		}

		if (!tempInput.isEmpty()) {
			addSubjob(baseJDL, tempInput, currentSize);
		}

		return subjobs;
	}

	private void addSubjob(final JDL baseJDL, final List<String> tempInput, final long currentSize) throws IllegalArgumentException {
		final JDL tmpJdl = changeFilePattern(baseJDL, tempInput);
		tmpJdl.addRequirement(setSizeRequirements(currentSize));
		tmpJdl.set("InputData", tempInput);
		subjobs.add(tmpJdl);
	}

}
