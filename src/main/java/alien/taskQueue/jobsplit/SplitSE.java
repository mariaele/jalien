package alien.taskQueue.jobsplit;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import alien.catalogue.LFN;
import alien.catalogue.LFNUtils;
import alien.catalogue.PFN;
import alien.se.SEUtils;
import alien.taskQueue.JDL;
import alien.taskQueue.TaskQueueUtils;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public class SplitSE extends JobSplitter {
	/**
	 * queue id for masterjob
	 */
	private long masterId;

	/**
	 * list of subjobs to submit
	 */
	private final List<JDL> subjobs = new ArrayList<>();

	/**
	 * @param strategy
	 */
	public SplitSE(final String strategy) {
		super.strategy = strategy;
	}

	/**
	 * Group inputdata files into basket based on locality
	 *
	 * @param mJDL masterjob JDL
	 * @param maxinputfiles
	 * @return Map of baskets and inputdata files of each basket
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	public Map<String, List<LFN>> groupInputFiles(final JDL mJDL, final int maxinputfiles) throws IOException, IllegalArgumentException {

		final Map<String, List<LFN>> groupedFiles = new HashMap<>();
		final Collection<LFN> inputFiles = mJDL.getInputLFNs();

		checkQuota(inputFiles.size(), maxinputfiles);

		checkNoDownload();

		if (inputFiles.isEmpty())
			throw new IOException("Could not find inputdata files!");

		final Map<LFN, Set<PFN>> lfnToPfn = LFNUtils.getRealPFNs(inputFiles);

		final Map<LFN, String> errorTrace = new HashMap<>();

		if (lfnToPfn.isEmpty())
			throw new IOException("Did not get any PFNs from LFNs");

		for (final LFN lfn : inputFiles) {
			if (!lfnToPfn.containsKey(lfn))
				errorTrace.put(lfn, "PFN does not exist");

			else if (lfnToPfn.get(lfn).isEmpty()) {
				errorTrace.put(lfn, "PFN set is empty");
			}

			else {
				final List<String> seNumber = new ArrayList<>();

				for (final PFN pfn : lfnToPfn.get(lfn)) {
					seNumber.add(Integer.toString(pfn.seNumber));
				}

				Collections.sort(seNumber);
				final String allSe = String.join(",", seNumber);
				if (!groupedFiles.containsKey(allSe))
					groupedFiles.put(allSe, new ArrayList<>());

				groupedFiles.get(allSe).add(lfn);

			}

		}
		if (!errorTrace.isEmpty()) {
			if (errorTrace.size() >= 5)
				TaskQueueUtils.putJobLog(masterId, "error", "Did not find physical locations for " + errorTrace.size(), null);
			else {
				for (final Map.Entry<LFN, String> entry : errorTrace.entrySet()) {
					TaskQueueUtils.putJobLog(masterId, "error", entry.getValue() + " for LFN " + entry.getKey(), null);
				}
			}
		}

		if (groupedFiles.isEmpty())
			throw new IOException("Issue grouping inputdata files into baskets based on SE");

		return groupedFiles;
	}

	@Override
	List<JDL> splitJobs(final JDL j, final long lMasterId) throws IOException, IllegalArgumentException {
		masterJdl = j;
		this.masterId = lMasterId;

		int maxinputnumber = 0;
		long maxfilesize = 0;
		int mininputnumber = 1;

		if ("file".equals(strategy)) {
			maxinputnumber = 1;
		}
		else {

			if (j.gets("SplitMaxInputFileNumber") != null) {
				maxinputnumber = Integer.parseInt(j.gets("SplitMaxInputFileNumber"));
			}
			else if (j.gets("SplitMaxInputFileSize") != null) {
				maxfilesize = j.getSize("SplitMaxInputFileSize", 'G', 1);
			}
			else
				throw new IllegalArgumentException("Splitting by SE requires the field 'SplitMaxInputFileNumber' or 'SplitMaxInputFileSize'");

			if (j.gets("SplitMinInputFileNumber") != null) {
				mininputnumber = Integer.parseInt(j.gets("SplitMinInputFileNumber"));
			}
			else
				mininputnumber = maxinputnumber / 5;
		}

		final Map<String, List<LFN>> groupInputFiles = groupInputFiles(j, maxinputnumber > 0 ? maxinputnumber : 1);

		/*
		 * String mtime = TaskQueueUtils.updateJobTime(masterId);
		 * if (mtime == null || mtime.equals(""))
		 * logger.log(Level.WARNING, "SplitSE could not update mtime");
		 * else
		 * this.setMtime(mtime);
		 */

		Map<String, List<String>> inputFilesLow = new HashMap<>();

		final JDL baseJDL = prepareSubJobJDL(j, lMasterId);

		if (masterJdl.gets("OrderLFN") != null) {
			sortStrategy = masterJdl.gets("OrderLFN");
		}

		try {
			for (final Entry<String, List<LFN>> entry : groupInputFiles.entrySet()) {
				if (sortStrategy != null)
					entry.setValue(sortLFNs(entry.getValue()));

				if (maxfilesize > 0) {
					List<String> tempInput = new ArrayList<>();
					long currentSize = 0;
					for (final LFN file : entry.getValue()) {
						if (maxfilesize < currentSize + file.getSize() && !tempInput.isEmpty()) {
							finalizingJDL(baseJDL, tempInput, entry.getKey());
							tempInput = new ArrayList<>();
							currentSize = 0;
						}

						currentSize += file.getSize();
						tempInput.add(getFinalLfnString(file.getCanonicalName()));

					}
					if (!tempInput.isEmpty()) {
						finalizingJDL(baseJDL, tempInput, entry.getKey());
					}

				}

				final List<String> inputdata = entry.getValue().stream()
						.map(LFN::getCanonicalName)
						.map(this::getFinalLfnString)
						.collect(Collectors.toList());

				if (inputdata.size() < mininputnumber) {
					inputFilesLow.put(entry.getKey(), inputdata);
				}
				else if (inputdata.size() > maxinputnumber) {
					int numberGroups = (int) Math.ceil((double) inputdata.size() / maxinputnumber);
					int tmpMax = inputdata.size() / numberGroups;
					int currentnumber = 0;
					List<String> tempInput = new ArrayList<>();

					for (final String file : inputdata) {

						if (currentnumber >= tmpMax && currentnumber != 0) {
							numberGroups--;
							tmpMax = ((inputdata.size() - currentnumber) / numberGroups) + currentnumber;
							finalizingJDL(baseJDL, tempInput, entry.getKey());
							tempInput = new ArrayList<>();

						}
						tempInput.add(file);
						currentnumber++;
					}

					if (!tempInput.isEmpty()) {
						finalizingJDL(baseJDL, tempInput, entry.getKey());
					}
				}
				else {

					finalizingJDL(baseJDL, inputdata, entry.getKey());
				}

			}

			if (!inputFilesLow.isEmpty()) {
				inputFilesLow = groupJobs(inputFilesLow, mininputnumber);
				for (final Entry<String, List<String>> entry : inputFilesLow.entrySet()) {
					finalizingJDL(baseJDL, entry.getValue(), entry.getKey());
				}
			}
		}
		catch (final Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			throw new IOException("Could not match baskets, " + sw.toString());
		}
		return subjobs;
	}

	/**
	 * Finalize the subjob JDL
	 *
	 * @param baseJDL base subjob JDL
	 * @param input List of inputdata files
	 * @param seList List of SE for basket/subjob
	 */
	private void finalizingJDL(final JDL baseJDL, final List<String> input, final String seList) throws IllegalArgumentException {
		final JDL tmpJdl = changeFilePattern(baseJDL, input);
		tmpJdl.addRequirement(setSizeRequirements(0));
		tmpJdl.set("InputData", input);
		final String[] ses = seList.split(",");
		StringBuffer buf = new StringBuffer();

		for (final String se : ses) {
			buf.append(String.format("member(other.CloseSE,\"%s\") || ", SEUtils.getSE(Integer.parseInt(se)).getName()));
		}

		String newReq = buf.toString();
		newReq = newReq.replaceFirst("\\s*\\|\\|\\s*$", "");
		tmpJdl.addRequirement(newReq);
		subjobs.add(tmpJdl);
	}

	/**
	 * Merge baskets that fall under the minimum threshold for inputdata files number
	 *
	 * @param inputFilesLow Map of baskets and inputdata files to be merged
	 * @param mininputnumber Minimum number of inputdata files per subjob
	 * @return Map of merged baskets and inputdata files
	 */
	public Map<String, List<String>> groupJobs(final Map<String, List<String>> inputFilesLow, final int mininputnumber) {
		final Map<String, List<String>> ret = new HashMap<>();

		String minBasket = "";
		while (!inputFilesLow.isEmpty()) {
			int min = 0;
			for (final Entry<String, List<String>> entry : inputFilesLow.entrySet()) {
				if (min > entry.getValue().size() || min == 0) {
					min = entry.getValue().size();
					minBasket = entry.getKey();
				}
			}
			String closeSE = "";
			try {
				closeSE = getCloseMaxShared(minBasket, inputFilesLow, mininputnumber);
			}
			catch (final Exception e) {
				TaskQueueUtils.putJobLog(masterId, "error", "Can't merge basket " + minBasket + ": " + e.getMessage() + ", staying seperate basket", null);
			}

			if ("".equals(closeSE)) {
				ret.put(minBasket, inputFilesLow.get(minBasket));
				inputFilesLow.remove(minBasket);

			}
			else {
				inputFilesLow.get(closeSE).addAll(inputFilesLow.get(minBasket));
				inputFilesLow.remove(minBasket);

				if (inputFilesLow.get(closeSE).size() > mininputnumber) {
					ret.put(closeSE, inputFilesLow.get(closeSE));
					inputFilesLow.remove(closeSE);
				}
			}
		}

		return ret;
	}

	/**
	 * Finding largest and closest basket to merge
	 *
	 * @param minBasket basket to be merged
	 * @param seListLow Map of baskets and inputdata files
	 * @param minReq Minimum number of inputdata files per subjob
	 * @return basket to merge with
	 */
	public static String getCloseMaxShared(final String minBasket, final Map<String, List<String>> seListLow, final int minReq) {

		String closest = "";
		int maxBasket = 0;
		boolean foundMatch = false;
		double minDist = 0.0;

		boolean foundShared = false;
		final String[] seMinBasket = minBasket.split(",");
		for (final Entry<String, List<String>> entry : seListLow.entrySet()) {
			if (!(entry.getKey().equals(minBasket))) {
				for (final String s : seMinBasket) {
					if (entry.getKey().contains(s)) {
						foundShared = true;
						if (!foundMatch) {
							if (entry.getValue().size() < minReq) {
								maxBasket = entry.getValue().size();
								closest = entry.getKey();
								foundMatch = true;
							}
							else if (maxBasket < entry.getValue().size()) {
								maxBasket = entry.getValue().size();
								closest = entry.getKey();
							}

							foundMatch = true;
							maxBasket = entry.getValue().size();
							closest = entry.getKey();
						}
						else if (maxBasket < entry.getValue().size()) {
							maxBasket = entry.getValue().size();
							closest = entry.getKey();
						}
					}
				}
			}
		}

		if (!foundShared) {
			for (final Entry<String, List<String>> entry : seListLow.entrySet()) {
				if (!(entry.getKey().equals(minBasket))) {
					double tmpDist = 0.0;
					final String[] tmp2 = entry.getKey().split(",");
					for (String s : seMinBasket) {
						s = SEUtils.getSE(Integer.parseInt(s)).getName();
						s = s.replaceFirst("::[^:]*$", "");
						s = s.replaceFirst("^[^:]*::", "");
						for (final String s2 : tmp2) {
							try {
								tmpDist += SEUtils.getDistance(s, Integer.valueOf(s2), false, true).doubleValue();
							}
							catch (final Exception e) {
								throw new NullPointerException("Can't merge basket " + s + " and  " + s2 + ", null");
							}
						}
					}
					if (minDist > tmpDist && !(entry.getKey().equals(minBasket))) {
						minDist = tmpDist;
						closest = entry.getKey();
					}
				}
			}
		}

		return closest;
	}

}
