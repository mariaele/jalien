package alien.taskQueue.jobsplit;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.config.ConfigUtils;
import alien.taskQueue.JDL;
import alien.taskQueue.Job;
import alien.taskQueue.JobStatus;
import alien.taskQueue.TaskQueueUtils;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public class JobSplitHelper implements Runnable {

	/**
	 * Logging facilities
	 */
	static final Logger logger = ConfigUtils.getLogger(TaskQueueUtils.class.getCanonicalName());

	private Job mJob;

	private final long masterId;
	private boolean checkCleanup = false;

	/**
	 * @param mJob
	 */
	public JobSplitHelper(final Job mJob) {
		this.mJob = mJob;
		this.masterId = mJob.queueId;
	}

	/**
	 * @param queueId
	 */
	public JobSplitHelper(final long queueId) {
		this.masterId = queueId;
	}

	@Override
	public void run() {
		try {
			if (mJob == null) {
				mJob = TaskQueueUtils.getJob(masterId);
			}
			if (mJob == null) {
				TaskQueueUtils.putJobLog(masterId, "error", "Failed getting Job class", null);
				TaskQueueUtils.setJobStatus(masterId, JobStatus.ERROR_SPLT);
				return;
			}

			if (checkCleanup) {
				if (!TaskQueueUtils.cleanupSubjobs(mJob)) {
					logger.info("Failed cleaning up old subjobs from a previous attempt for job " + masterId);
					TaskQueueUtils.putJobLog(masterId, "error", "Failed cleaning up old subjobs from a previous attempt", null);
					TaskQueueUtils.setJobStatus(masterId, JobStatus.ERROR_SPLT);
				}
			}

			JDL j = null;
			int retry = 0;
			String jdlString = null;
			while (jdlString == null) {
				jdlString = mJob.getJDL();

				if (jdlString == null) {
					retry++;
					if (retry <= 5) {

						try {
							Thread.sleep(1 * 1000);
						}
						catch (final InterruptedException e) {
							throw new RuntimeException(e);
						}

					}
					else {
						TaskQueueUtils.putJobLog(masterId, "error", "Failed getting JDL for the job", null);
						TaskQueueUtils.setJobStatus(masterId, JobStatus.ERROR_SPLT);
						return;
					}
				}

			}
			j = new JDL(mJob.getJDL());

			TaskQueueUtils.putJobLog(masterId, "trace", "Split by jalien @ " + ConfigUtils.getLocalHostname(), null);

			if (!mJob.masterjob) {
				TaskQueueUtils.insertToWaiting(mJob, j);
				return;
			}

			JobSplitter split;

			final String strategy = j.gets("Split");

			if (strategy == null) {
				throw new Exception("split strategy not found");
			}

			switch (strategy.toLowerCase()) {
				case "parentdirectory":
					split = new SplitParse(strategy, "/[^/]*/?[^/]*$");
					break;
				case "directory":
					split = new SplitParse(strategy, "/[^/]*$");
					break;
				case "file":
					split = new SplitSE("file");
					break;
				case "se":
					split = new SplitSE("se");
					break;
				case "af":
					split = new SplitAF("af");
					break;
				case "directaccess":
					split = new SplitDirectAccess("directaccess");
					break;
				case "custom":
					split = new SplitCustom();
					break;
				default:
					if (strategy.startsWith("production:")) {
						split = new SplitProduction(strategy);
					}
					else if (strategy.startsWith("se:")) {
						split = new SplitSE(strategy);
					}
					else {
						throw new IllegalArgumentException("Split strategy not found");
					}
			}

			logger.log(Level.INFO,
					"[Testing Job Optimizer] Trying to split queueId " + masterId + " by stategy " + strategy);
			List<JDL> subjobs = split.splitJobs(j, masterId);
			if (subjobs.isEmpty()) {
				throw new Exception("Subjobs is empty!");
			}
			TaskQueueUtils.insertSubJob(mJob, subjobs, j);

		}
		catch (final IllegalArgumentException e) {
			logger.log(Level.INFO, "Failed splitting the job " + masterId, e);
			TaskQueueUtils.putJobLog(masterId, "error", "Failed splitting the job " + masterId + ", " + e.getMessage(),
					null);
			TaskQueueUtils.setJobStatus(masterId, JobStatus.INCORRECT);
		}
		catch (final Exception e) {
			logger.log(Level.INFO, "Failed splitting job  " + masterId, e);
			TaskQueueUtils.putJobLog(masterId, "error", "Failed splitting the job " + masterId + " , " + e.getMessage(),
					null);
			TaskQueueUtils.setJobStatus(masterId, JobStatus.ERROR_SPLT);
		}

	}

	/**
	 * Set this masterjob back to INSERTING for later reprocessing
	 */
	public void setJobStatus() {
		TaskQueueUtils.setJobStatus(masterId, JobStatus.INSERTING);
	}

	/**
	 * After a failed attempt to split it, the code should try to clean up the subjobs potentially produced by the previous iteration
	 */
	public void setCleanupCheck() {
		checkCleanup = true;
	}
}
