package alien.taskQueue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.nfunk.jep.JEP;

import alien.api.Dispatcher;
import alien.api.taskQueue.CE;
import alien.api.token.GetTokenCertificate;
import alien.api.token.TokenCertificateType;
import alien.catalogue.PackageUtils;
import alien.config.ConfigUtils;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.priority.PriorityRegister;
import alien.site.KillReason;
import alien.site.SiteMap;
import alien.site.packman.CVMFS;
import alien.user.AliEnPrincipal;
import lazyj.DBFunctions;
import lazyj.DBFunctions.DBConnection;
import lazyj.Format;
import utils.DBUtils;

/**
 *
 */
public class JobBroker {

	/**
	 * Logger
	 */
	static final Logger logger = ConfigUtils.getLogger(JobBroker.class.getCanonicalName());

	/**
	 * Monitoring component
	 */
	static final Monitor monitor = MonitorFactory.getMonitor(JobBroker.class.getCanonicalName());

	private static long lastCVMFSRevisionCheck = 0;
	private static long lastCVMFSRevisionModified = 0;
	private static int lastCVMFSRevision = 0;

	/**
	 * @return the cached CVMFS revision on the server side
	 */
	public static synchronized int getCachedCVMFSRevision() {
		if (lastCVMFSRevisionCheck < System.currentTimeMillis() || lastCVMFSRevision <= 0) {
			final int cvmfsRevision = CVMFS.getRevision();

			if (cvmfsRevision < 0) {
				logger.log(Level.WARNING, "Could not get the CVMFS revision");
				lastCVMFSRevisionCheck = System.currentTimeMillis() + 1000 * 15;
			}
			else {
				if (cvmfsRevision > lastCVMFSRevision) {
					lastCVMFSRevisionModified = System.currentTimeMillis();
				}

				lastCVMFSRevision = cvmfsRevision;
				lastCVMFSRevisionCheck = System.currentTimeMillis() + 1000 * 60;
			}
		}

		return lastCVMFSRevision;
	}

	/**
	 * @param matchRequest
	 * @return the information of a jobAgent (entryId)
	 *         that matches the request when "Site" is not considered
	 */
	private static HashMap<String, Object> getMatchJobNoSite(final HashMap<String, Object> matchRequest) {
		HashMap<String, Object> matchRequestClone = new HashMap<>(matchRequest);

		matchRequestClone.put("Remote", Integer.valueOf(1));
		matchRequestClone.remove("Site");
		matchRequestClone.remove("Extrasites");

		return getNumberWaitingForSite(matchRequestClone);
	}

	/**
	 * @param matchRequest
	 * @param isRemote
	 * @return the information of a jobAgent (entryId)
	 *         that matches the request when TTL is optimised.
	 *         If isRemote is true, then "Site" will not be considered (see getMatchJobNoSite)
	 */
	private static HashMap<String, Object> getMatchJobTTL(final HashMap<String, Object> matchRequest, boolean isRemote) {
		HashMap<String, Object> matchRequestClone = new HashMap<>(matchRequest);
		HashMap<String, Object> waiting = new HashMap<>();

		long requestedTTL = ((Number) matchRequestClone.get("TTL")).longValue();
		matchRequestClone.remove("TTL");
		matchRequestClone.put("Return", "entryId,productionId,TTLOptimizationType");
		matchRequestClone.put("OptimizeTTL", Boolean.valueOf(true));
		matchRequestClone.put("limit", Integer.valueOf(10));

		if (isRemote)
			waiting = getMatchJobNoSite(matchRequestClone);
		else
			waiting = getNumberWaitingForSite(matchRequestClone);

		if (waiting.containsKey("entries")) {
			logger.log(Level.INFO, "We have jobs that might be optimised (TTL)");
			try {
				@SuppressWarnings("unchecked")
				Collection<Map<String, Object>> entries = (Collection<Map<String, Object>>) waiting.get("entries");
				for (Map<String, Object> entry : entries) {

					final Integer agentId = Integer.valueOf((String) entry.get("entryId"));
					final Long productionId = Long.valueOf((String) entry.get("productionId"));
					final String ttlOptimizationType = (String) entry.get("TTLOptimizationType");
					final String cpuModelName = (String) matchRequest.get("cpuModelName");
					final String site = (String) matchRequest.get("Site");

					if (agentId == null || productionId == null || ttlOptimizationType == null
							|| cpuModelName == null || site == null) {
						final String errorMessage = "Errors while parsing entry for TTL optimization: " + entry;
						logger.log(Level.SEVERE, errorMessage);
						waiting.put("Error", errorMessage);
						waiting.put("Code", Integer.valueOf(-7));
						return waiting;
					}

					entry.put("reqTTL", Long.valueOf(requestedTTL));
					entry.put("cpuModelName", cpuModelName);
					entry.put("Site", site);

					logger.log(Level.FINE, "We'll try to optimize the TTL using " + ttlOptimizationType
							+ " for agentId " + agentId + ", productionId " + productionId
							+ ", site " + site + ", cpuModelName " + cpuModelName
							+ ", remaining TTL on slot " + requestedTTL);
					final Long weightedTTL = TaskQueueUtils.getEstimatedTTL(ttlOptimizationType, entry);

					if (weightedTTL != null && weightedTTL.longValue() < requestedTTL) {
						logger.log(Level.INFO, "We might have a job to give back. Remaining TTL on slot: "
								+ requestedTTL + " and estimated TTL is " + weightedTTL);

						waiting.remove("entries");
						waiting.putAll(entry);
						waiting.put("optimizedTTL", weightedTTL);
						break;
					}
				}
			}
			catch (Exception e) {
				String errorMessage = "No TTL optimizations could be applied. Errors while parsing the entries.";
				logger.log(Level.SEVERE, errorMessage + "Errors: " + e);
				waiting.put("Error", errorMessage);
				waiting.put("Code", Integer.valueOf(-8));
			}
		}
		else {
			String errorMessage = "No TTL optimizations could be applied.";
			logger.log(Level.INFO, errorMessage);
			waiting.put("Error", errorMessage);
			waiting.put("Code", Integer.valueOf(-9));
		}

		return waiting;
	}

	private static void incrementMonitorCounter(final String key) {
		if (monitor != null) {
			monitor.incrementCounter(key);
		}
	}

	/**
	 * @param matchRequest
	 * @return the information of a matching job for the jobAgent (queueId,
	 *         JDL...)
	 */
	public static HashMap<String, Object> getMatchJob(final HashMap<String, Object> matchRequest) {
		incrementMonitorCounter("TQ_get_match_job");

		if (!ConfigUtils.getConfig().getb("alien.taskQueue.JobBroker.enabled", true)) {
			final HashMap<String, Object> matchAnswer = new HashMap<>();
			final String errorMessage = "JobBroker is disabled, draining the Grid of jobs";
			matchAnswer.put("Error", errorMessage);
			matchAnswer.put("Code", Integer.valueOf(-1));

			logger.log(Level.FINE, errorMessage);

			return matchAnswer;
		}

		updateWithValuesInLDAP(matchRequest);

		final Object workerNodeCVMFSRevision = matchRequest.get("CVMFS_revision");

		if (workerNodeCVMFSRevision != null && workerNodeCVMFSRevision instanceof Integer) {
			final int wnCVMFSRevision = ((Integer) workerNodeCVMFSRevision).intValue();
			final int serverCVMFSRevision = getCachedCVMFSRevision();

			boolean oldCVMFSRevision = wnCVMFSRevision < serverCVMFSRevision;

			if (oldCVMFSRevision && (System.currentTimeMillis() - lastCVMFSRevisionModified < 1000L * 60 * 60 && wnCVMFSRevision == serverCVMFSRevision - 1)) {
				// allow one hour propagation delay
				oldCVMFSRevision = false;
			}

			if (oldCVMFSRevision) {
				final String errorMessage = "The node has an outdated CVMFS revision "
						+ wnCVMFSRevision + ", server has " + serverCVMFSRevision;
				logger.log(Level.WARNING, errorMessage + " for "
						+ Format.toInterval(System.currentTimeMillis() - lastCVMFSRevisionModified)
						+ ":\n" + matchRequest);

				final HashMap<String, Object> matchAnswer = new HashMap<>(2);
				matchAnswer.put("Error", errorMessage);
				matchAnswer.put("Code", Integer.valueOf(-2));
				return matchAnswer;
			}
		}

		try (DBFunctions db = TaskQueueUtils.getQueueDB()) {
			if (db == null)
				return null;

			db.setQueryTimeout(300);

			HashMap<String, Object> matchAnswer = new HashMap<>();
			HashMap<String, Object> waiting = new HashMap<>();

			logger.log(Level.INFO, "We received parameters: " + matchRequest.toString());

			if (!matchRequest.containsKey("AliEnPrincipal")) {
				final String errorMessage = "getMatchJob: AliEnPrincipal field missing";
				logger.log(Level.SEVERE, errorMessage);
				matchAnswer.put("Error", errorMessage);
				matchAnswer.put("Code", Integer.valueOf(-3));
				setRejectionReason(errorMessage, String.valueOf(matchRequest.get("CE")));
				return matchAnswer;
			}

			// Checking if the CE is open
			final int openQueue = checkQueueOpen((String) matchRequest.get("CE"));
			if (openQueue != 1) {
				final String errorMessage = "Queue is not open! Check queueinfo";
				logger.log(Level.INFO, errorMessage);
				matchAnswer.put("Error", errorMessage);
				matchAnswer.put("Code", Integer.valueOf(-4));
				setRejectionReason(errorMessage, String.valueOf(matchRequest.get("CE")));
				return matchAnswer;
			}

			matchRequest.put("Remote", Integer.valueOf(0));
			matchRequest.put("Return", "entryId"); // skipping ,filebroker

			if (!matchRequest.containsKey("Partition"))
				matchRequest.put("Partition", ",,");

			if (!TaskQueueUtils.updateHostStatus((String) matchRequest.get("Host"), "ACTIVE")) {
				final String errorMessage = "Updating host failed!";
				logger.log(Level.INFO, errorMessage);
				matchAnswer.put("Error", errorMessage);
				matchAnswer.put("Code", Integer.valueOf(-5));
				setRejectionReason(errorMessage, String.valueOf(matchRequest.get("CE")));
				return matchAnswer;
			}

			String monitoringKey = "Broker_Match_Full";
			waiting = getNumberWaitingForSite(matchRequest);

			if (!waiting.containsKey("entryId")) {
				logger.log(Level.INFO, "Going to try to optimize TTL");
				monitoringKey = "Broker_Match_TTL";
				waiting = getMatchJobTTL(matchRequest, false);
			}

			if (!waiting.containsKey("entryId")) {
				logger.log(Level.INFO, "Going to try with remote execution agents");
				 monitoringKey = "Broker_Match_Remote";
				waiting = getMatchJobNoSite(matchRequest);
			}

			if (!waiting.containsKey("entryId")) {
				logger.log(Level.INFO, "Going to try to optimize TTL with remote execution agents");
				monitoringKey = "Broker_Match_TTL_Remote";
				waiting = getMatchJobTTL(matchRequest, true);
			}

			if (!waiting.containsKey("entryId")) {
				logger.log(Level.INFO, "Optimizing the TTL and/or removing site hasn't been enough. Nothing to run!");
				matchAnswer.put("Error", "Nothing to run");
				matchAnswer.put("Code", Integer.valueOf(-6));
				TaskQueueUtils.setSiteQueueStatus((String) matchRequest.get("CE"), "jobagent-no-match");
				monitoringKey = "Broker_No_Match";
				incrementMonitorCounter(monitoringKey);
			}

			if (matchAnswer.containsKey("Error")) {
				setRejectionReason(String.valueOf(matchAnswer.get("Error")), String.valueOf(matchRequest.get("CE")));
				return matchAnswer;
			}

			logger.log(Level.INFO, "We have a job back");
			matchAnswer = getWaitingJobForAgentId(waiting);

			// we get back the needed information
			if (matchAnswer.containsKey("queueId")) {
				// success!!
				matchAnswer.put("Code", Integer.valueOf(1));
				final Long queueId = (Long) matchAnswer.get("queueId");
				final String username = (String) matchAnswer.get("User");

				int resubmission = ((Integer) matchAnswer.getOrDefault("Resubmission", Integer.valueOf(-1))).intValue();
				final JobToken jobToken = TaskQueueUtils.insertJobToken(queueId.longValue(), (String) matchAnswer.get("User"), resubmission);
				if (jobToken != null) {
					resubmission = jobToken.resubmission;

					if (jobToken.legacyToken != null && jobToken.legacyToken.length() == 32)
						matchAnswer.put("LegacyToken", jobToken.legacyToken);
				}

				if (resubmission >= 0) {
					GetTokenCertificate gtc = new GetTokenCertificate((AliEnPrincipal) matchRequest.get("AliEnPrincipal"), username, TokenCertificateType.JOB_TOKEN,
							"queueid=" + queueId + "/resubmission=" + resubmission, 1);
					try {
						gtc = Dispatcher.execute(gtc);
						matchAnswer.put("TokenCertificate", gtc.getCertificateAsString());
						matchAnswer.put("TokenKey", gtc.getPrivateKeyAsString());
					}
					catch (final Exception e) {
						logger.info("Getting TokenCertificate for job " + queueId + " failed: " + e);
					}
				}

				if (!matchAnswer.containsKey("TokenCertificate") || !matchAnswer.containsKey("TokenKey")) {
					logger.log(Level.INFO, "The job could not create token: " + queueId);

					db.setReadOnly(true);

					final Map<String, Object> info = Map.of("killreason", (Object) (Integer.valueOf(KillReason.JOBBROKER.getCode())), "error", Integer.valueOf(2));

					TaskQueueUtils.setJobStatus(queueId.longValue(), JobStatus.ERROR_A, null, info, null);

					matchAnswer.put("Code", Integer.valueOf(-7));
					matchAnswer.put("Error", "Error getting the TokenCertificate of the job " + queueId);

					TaskQueueUtils.putJobLog(queueId.longValue(), "trace", String.valueOf(matchAnswer.get("Error")), null);

					setRejectionReason(String.valueOf(matchAnswer.get("Error")), String.valueOf(matchRequest.get("CE")));
					if (jobToken != null)
						jobToken.destroy(db);

					incrementMonitorCounter("Broker_Error_Token_Certificate");
				}
				else {
					logger.log(Level.INFO, "Created a TokenCertificate for the job...");
					TaskQueueUtils.setSiteQueueStatus((String) matchRequest.get("CE"), "jobagent-match");
					TaskQueueUtils.putJobLog(queueId.longValue(), "state", "Job ASSIGNED to: " + (String) matchRequest.get("CE"), null);

					// We gave a job back, so we can increase the counter
					incrementMonitorCounter(monitoringKey);
				}
			} // nothing back, something went wrong while obtaining queueId
				// from the positive cases
			else if (!matchAnswer.containsKey("Code")) {
				matchAnswer.put("Error", "Nothing to run :-( (no waiting jobs?) ");
				matchAnswer.put("Code", Integer.valueOf(-6));
				TaskQueueUtils.setSiteQueueStatus((String) matchRequest.get("CE"), "jobagent-no-match");
				setRejectionReason(String.valueOf(matchAnswer.get("Error")), String.valueOf(matchRequest.get("CE")));
				incrementMonitorCounter("Broker_No_Waiting_jobs");
			}

			return matchAnswer;
		}
	}

	private static void setRejectionReason(String error, String ce) {
		try (DBFunctions db = TaskQueueUtils.getQueueDB()) {
			if (db == null)
				return;
			long timestamp = System.currentTimeMillis();
			String q = "update SITEQUEUES set lastRejectionTime=" + timestamp + ", lastRejectionReason=\'" + error + "\' where site=\'" + ce + "\'";
			db.query(q);
		}
	}

	/**
	 * @param jobId
	 * @return the list of matching CEs
	 */
	public static HashMap<CE, Object> getMatchingCEs(final long jobId) {
		HashMap<CE, Object> matchingCEs = new HashMap<>();
		logger.log(Level.INFO, "Getting matching CEs for jobId " + jobId);
		try (DBFunctions db = TaskQueueUtils.getQueueDB(); DBFunctions db2 = TaskQueueUtils.getQueueDB()) {
			if (db == null || db2 == null)
				return null;
			db.query("SELECT agentId FROM QUEUE WHERE queueId = " + jobId + ";");
			if (db.moveNext()) {
				int jobAgentId = db.geti("agentId");
				logger.log(Level.INFO, "Job " + jobId + " has the agentID " + jobAgentId);
				db.query("SELECT entryId, ce, noce, ttl,  user, packages, cpucores, site, `partition` FROM JOBAGENT join QUEUE_USER on JOBAGENT.userId=QUEUE_USER.userId WHERE entryId = " + jobAgentId
						+ ";");
				if (db.moveNext()) {
					db2.query("SELECT site, maxrunning, maxqueued, blocked FROM SITEQUEUES WHERE blocked='open';", false);
					while (db2.moveNext()) {
						CE candidateCE = new CE(db2.gets("site"), db2.geti("maxrunning"), db2.geti("maxqueued"), db2.gets("blocked"));
						if (db.gets("noce").toUpperCase().contains("," + candidateCE.ceName + ",")) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. It is in the noce list.");
							continue;
						}

						if (candidateCE.TTL < db.geti("ttl")) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. TTL too small (" + candidateCE.TTL + ")");
							continue;
						}

						if (!(db.gets("partition").equals("%") || candidateCE.partitions.contains(db.gets("partition")))) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. Partition not matched (" + candidateCE.partitions + ")");
							continue;
						}

						if (db.geti("cpucores") > candidateCE.matchCpuCores && candidateCE.matchCpuCores != 0) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. Requested CPU cores not available (" + candidateCE.matchCpuCores + ")");
							continue;
						}

						if (!(db.gets("site").isBlank() || db.gets("site").toUpperCase().contains(candidateCE.site))) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. Site not matched (" + candidateCE.site + ")");
							continue;
						}

						if (!(db.gets("ce").equals("") || db.gets("ce").toUpperCase().contains("," + candidateCE.ceName + ","))) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. It is in the noce list.");
							continue;
						}

						if (!(candidateCE.nousers.isEmpty() || !candidateCE.nousers.contains(db.gets("user")))) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. The user is not allowed in the CE (" + db.gets("user") + ")");
							continue;
						}

						if (!(candidateCE.users.isEmpty() || candidateCE.users.contains(db.gets("user")))) {
							logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. The user is not in the allowed users list (" + db.gets("user") + ")");
							continue;
						}

						if (!candidateCE.requiredCpuCores.equals("")) {
							JEP jep = new JEP();
							jep.addVariable("cpucores", db.geti("cpucores"));
							jep.addStandardFunctions();
							jep.parseExpression("if (cpucores" + candidateCE.requiredCpuCores + ", 1, 0)");
							if (Integer.valueOf(0).equals(jep.getValueAsObject())) {
								logger.log(Level.INFO, "CE " + candidateCE.ceName + " was discarded. The required CPU cores of the CE were not met (" + candidateCE.requiredCpuCores + ")");
								continue;
							}
						}
						logger.log(Level.INFO, "The CE " + candidateCE.ceName + " would be able to run the job. Including it in the list");
						matchingCEs.put(candidateCE, null);
					}
				}
			}
		}
		return matchingCEs;
	}

	private static HashMap<String, Object> getWaitingJobForAgentId(final HashMap<String, Object> waiting) {
		try (DBFunctions db = TaskQueueUtils.getQueueDB()) {
			if (db == null)
				return null;

			db.setQueryTimeout(60);

			final Integer agentId = Integer.valueOf((String) waiting.get("entryId"));
			final String host = (String) waiting.get("Host");
			final String ceName = (String) waiting.get("CE");

			final int hostId = TaskQueueUtils.getOrInsertFromLookupTable("host", host);
			final int siteId = TaskQueueUtils.getSiteId(ceName);

			if (hostId == 0 || siteId == 0)
				logger.log(Level.INFO, "The value for " + (hostId > 0 ? "site" : "host") + " is missing");

			logger.log(Level.INFO, "Getting a waiting job for " + agentId + " and " + host + " and " + hostId + " and " + siteId);

			final HashMap<String, Object> job = new HashMap<>();

			String extra = "";
			if (waiting.containsKey("Remote") && ((Integer) waiting.get("Remote")).intValue() == 1)
				extra = " and timestampdiff(SECOND,mtime,now())>=ifnull(remoteTimeout,43200)";

			final DBConnection dbc = db.getConnection();

			long queueId = 0;

			final Connection conn = dbc.getConnection();

			try (Timing t = new Timing(monitor, "brokeringQTime")) {
				conn.setReadOnly(false);
				conn.setAutoCommit(false);

				final int prevTransactionIsolation = conn.getTransactionIsolation();

				if (prevTransactionIsolation != Connection.TRANSACTION_READ_COMMITTED)
					conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

				final String selectQuery = "SELECT queueId, statusId, siteId, execHostId, agentId FROM QUEUE WHERE statusId=" + JobStatus.WAITING.getAliEnLevel() +
						" AND agentId=?" + extra + " ORDER BY queueId ASC LIMIT 1 FOR UPDATE SKIP LOCKED;";

				try (PreparedStatement stat = conn.prepareStatement(selectQuery, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
					stat.setObject(1, agentId);
					stat.setQueryTimeout(60);
					stat.execute();

					try (ResultSet resultSet = stat.getResultSet()) {
						if (!resultSet.next()) {
							if (logger.isLoggable(Level.FINE)) {
								logger.log(Level.FINE, "Query didn't find any match for agentId " + agentId + "\n"
										+ DBUtils.applyBindParameters(selectQuery, List.of(agentId)));
							}

							logger.log(Level.INFO, "No jobs to give back");
							job.put("Error", "No jobs to give back");
							job.put("Code", Integer.valueOf(-2));
							return job;
						}

						queueId = resultSet.getLong(1);

						resultSet.updateInt("statusId", JobStatus.ASSIGNED.getAliEnLevel());
						resultSet.updateInt("siteId", siteId);
						resultSet.updateInt("execHostId", hostId);
						resultSet.updateObject("agentId", null);
						resultSet.updateRow();
					}
				}

				conn.commit();

				if (prevTransactionIsolation != Connection.TRANSACTION_READ_COMMITTED)
					conn.setTransactionIsolation(prevTransactionIsolation);
			}
			catch (final SQLException e) {
				try {
					conn.rollback();
				}
				catch (SQLException ex) {
					logger.log(Level.WARNING, "Exception rolling back brokering transaction", ex);
					// throw new RuntimeException(ex);
				}

				logger.log(Level.INFO, "Some other SQL exception while updating the queue for agentId " + agentId + " and queueId " + queueId, e);
				job.put("Error", "Could not get you a job for agentId: " + agentId);
				job.put("Code", Integer.valueOf(-6));
				return job;
			}
			finally {
				try {
					conn.setAutoCommit(true);
					dbc.free();
				}
				catch (SQLException ex) {
					logger.log(Level.SEVERE, "Cannot reset the autocommit flag", ex);

					dbc.close();
				}
			}

			// we got something to run
			String jdl = null, user = null;
			int userId = -1, cpucores = -1;

			int resubmission = -1;

			for (int i = 0; i < 5; i++) {
				db.query("select origjdl jdl, user, resubmission, userId, cpucores from QUEUEJDL join QUEUE using (queueid) join QUEUE_USER using (userId) where queueId=?", false,
						Long.valueOf(queueId));

				if (db.moveNext()) {
					logger.log(Level.INFO, "Updated and getting fields queueId, jdl, user for queueId " + queueId);
					jdl = db.gets(1);
					user = db.gets(2);
					resubmission = db.geti(3);
					userId = db.geti(4);
					cpucores = db.geti(5);
					break;
				}

				try {
					Thread.sleep(1000);
				}
				catch (InterruptedException e) {
					logger.log(Level.INFO, "Interrupted while waiting for the JDL of " + queueId + " to show up", e);
					break;
				}
			}

			if (resubmission < 0) {
				logger.log(Level.INFO, "Couldn't get the queueId, jdl and user for the agentId: " + agentId);
				job.put("Error", "Couldn't get the queueId, jdl and user for the agentId: " + agentId);
				job.put("Code", Integer.valueOf(-7));

				TaskQueueUtils.putJobLog(queueId, "trace", String.valueOf(job.get("Error")), null);

				db.query("UPDATE QUEUE SET statusId=" + JobStatus.ERROR_A.getAliEnLevel() + ", killreason=" + KillReason.JOBBROKER.getCode() + ", error=3 WHERE queueId=" + queueId);

				return job;
			}

			db.query("INSERT INTO QUEUEPROC (queueId, lastupdate) VALUES (?, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE lastupdate=CURRENT_TIMESTAMP;", false, Long.valueOf(queueId));

			db.query("update SITEQUEUES set ASSIGNED=GREATEST(ASSIGNED,0)+1, WAITING=GREATEST(WAITING-1,0) where siteid=?", false, Integer.valueOf(siteId));

			TaskQueueUtils.deleteJobAgent(agentId.longValue(), queueId);

			job.put("queueId", Long.valueOf(queueId));
			job.put("User", user);
			job.put("Resubmission", Integer.valueOf(resubmission));

			try {
				final JDL j = new JDL(Job.sanitizeJDL(jdl));
				if (waiting.containsKey("optimizedTTL")) {
					job.put("optimizedTTL", waiting.get("optimizedTTL"));
					j.set("ProxyTTL", "true");
				}
				job.put("JDL", j.toString());

				final Collection<String> packages = j.getList("Packages");

				if (packages != null && packages.size() > 0) {
					Set<String> platforms = null;

					for (final String pEntry : packages) {
						final alien.catalogue.Package p = PackageUtils.getPackage(pEntry);

						if (p == null) {
							final String errorMessage = "Could not locate the package named " + pEntry;
							logger.log(Level.SEVERE, errorMessage);
							job.put("Error", errorMessage);
							job.put("Code", Integer.valueOf(-8));
							break;
						}

						if (p.getPlatforms().contains("source") || p.getPlatforms().contains("noarch"))
							continue;

						if (platforms == null)
							platforms = p.getPlatforms();
						else
							platforms.retainAll(p.getPlatforms());
					}

					if (platforms != null && platforms.size() > 0) {
						if (logger.isLoggable(Level.INFO))
							logger.log(Level.INFO, "Platforms for " + queueId + ": " + platforms);

						job.put("Platforms", String.valueOf(platforms));
					}
					else
						logger.log(Level.WARNING, "No common platforms for the packages of " + queueId + " : " + packages);
				}
			}
			catch (final Exception e) {
				final String errorMessage = "Exception parsing the JDL of " + queueId;
				logger.log(Level.SEVERE, errorMessage, e);
				job.put("Error", errorMessage);
				job.put("Code", Integer.valueOf(-9));
			}

			if (job.get("Error") != null) {
				job.remove("queueId");
				TaskQueueUtils.putJobLog(queueId, "trace", String.valueOf(job.get("Error")), null);

				final Map<String, Object> info = new HashMap<>(4);

				info.put("killreason", Integer.valueOf(KillReason.JOBBROKER.getCode()));
				info.put("error", Integer.valueOf(1));

				TaskQueueUtils.setJobStatus(queueId, JobStatus.ERROR_A, null, info, null);
				return job;
			}

			if (logger.isLoggable(Level.FINE))
				logger.log(Level.FINE, "Going to return " + queueId + " and " + user + " and " + jdl);

			PriorityRegister.JobCounter.getCounterForUser(Integer.valueOf(userId)).incRunningAndDecWaiting(cpucores);

			return job;
		}

	}

	private static int checkQueueOpen(final String ce) {
		try (DBFunctions db = TaskQueueUtils.getQueueDB()) {
			if (db == null)
				return -2;

			if (monitor != null) {
				monitor.incrementCounter("TQ_db_lookup");
				monitor.incrementCounter("TQ_queue_open");
			}

			db.setReadOnly(true);
			db.setQueryTimeout(30);

			if (db.query("select count(1) from SITEQUEUES where blocked='open' and site=?", false, ce) && db.moveNext() && db.geti(1) > 0)
				return 1;
			// TODO: use TaskQueueUtils.setSiteQueueStatus(ce,
			// "closed-blocked");

			return -1;
		}
	}

	private static String lastRemoteAgents = null;

	private static long lastRemoteAgentsChecked = 0;

	private static synchronized String getRemoteAgents() {
		if (System.currentTimeMillis() > lastRemoteAgentsChecked) {
			lastRemoteAgents = null;

			try (DBFunctions db = ConfigUtils.getDB("processes")) {
				db.setReadOnly(true);
				db.setQueryTimeout(60);

				if (db.query("select group_concat(distinct agentId) from QUEUE where agentId is not null and statusId=" + JobStatus.WAITING.getAliEnLevel()
						+ " and timestampdiff(SECOND,mtime,now())>=ifnull(remoteTimeout,43200)")
						&& db.moveNext())
					lastRemoteAgents = db.gets(1);
			}

			lastRemoteAgentsChecked = System.currentTimeMillis() + 1000L * 60;
		}

		return lastRemoteAgents;
	}

	/**
	 * @param matchRequest
	 * @param monitorKey the monitoring key to increment on match
	 * @return number of jobs waiting for a site given its parameters, or an
	 *         entry to JOBAGENT if asked for
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> getNumberWaitingForSite(final HashMap<String, Object> matchRequest) {
		final HashMap<String, Object> matchAnswer = new HashMap<>();
		matchAnswer.put("Code", Integer.valueOf(0));

		if (!ConfigUtils.getConfig().getb("alien.taskQueue.JobBroker.enabled", true)) {
			logger.log(Level.FINE, "JobBroker is disabled, draining the Grid of jobs");
			return matchAnswer;
		}

		updateWithValuesInLDAP(matchRequest);

		boolean isRemoteAccessAllowed = false;

		final Object remoteValue = matchRequest.get("Remote");

		if (remoteValue != null && (remoteValue instanceof Number))
			isRemoteAccessAllowed = ((Number) remoteValue).intValue() > 0;

		try (DBFunctions db = TaskQueueUtils.getQueueDB()) {
			if (db == null)
				return null;

			db.setQueryTimeout(60);

			String where = "";
			final String ret = (String) matchRequest.getOrDefault("Return", "sum(counter) as counter");

			final ArrayList<Object> bindValues = new ArrayList<>();

			if (matchRequest.containsKey("TTL")) {
				where += "and ttl < ? ";
				bindValues.add(matchRequest.get("TTL"));
			}

			if (matchRequest.containsKey("Disk")) {
				where += "and disk < ? ";
				bindValues.add(matchRequest.get("Disk"));
			}

			// Checks that if whole node scheduling, no constraint on CPUCores is added from the CE
			if (matchRequest.containsKey("CPUCores") && Integer.parseInt(matchRequest.get("CPUCores").toString()) != 0) {
				where += "and cpucores <= ? ";
				bindValues.add(matchRequest.get("CPUCores"));
			}

			if (matchRequest.containsKey("Memory")) {
				where += " and (coalesce(memory,2*1024*CPUCores) <= ?) ";
				bindValues.add(matchRequest.get("Memory"));
			}

			if (matchRequest.containsKey("Site")) {
				where += "and (site='' or site like concat('%,',?,',%')";
				bindValues.add(matchRequest.get("Site"));
			}

			if (matchRequest.containsKey("Extrasites")) {
				final ArrayList<String> extrasites = (ArrayList<String>) matchRequest.get("Extrasites");
				for (final String site : extrasites) {
					where += " or site like concat('%,',?,',%') ";
					bindValues.add(site);
				}
				where += ") ";
			}
			else if (matchRequest.containsKey("Site"))
				where += ") ";

			if (matchRequest.containsKey("CE")) {
				if (!isRemoteAccessAllowed) {
					// if remote access is allowed then the CE doesn't have to match any more, any site from the same partition can pick up the job
					// on the other hand if remote access is not allowed then the CE must match the requirements, so enforce it here
					where += " and (ce like '' or ce like concat('%,',?,',%'))";
					bindValues.add(matchRequest.get("CE"));
				}

				where += " and noce not like concat('%,',?,',%')";
				bindValues.add(matchRequest.get("CE"));
			}

			if (matchRequest.containsKey("Partition") && !",,".equals(matchRequest.get("Partition"))) {
				where += " and ? like concat('%,',`partition`, ',%') ";
				bindValues.add(matchRequest.get("Partition"));
			}
			else {
				// if the CE is not in any partition, it can only pick up jobs that have no partition restrictions
				where += " and `partition`='%' ";
			}

			final String CeRequirements = Objects.isNull(matchRequest.get("ce_requirements")) ? "" : matchRequest.get("ce_requirements").toString();

			matchRequest.putIfAbsent("Users", SiteMap.getFieldContentsFromCerequirements(CeRequirements, SiteMap.CE_FIELD.Users));
			matchRequest.putIfAbsent("NoUsers", SiteMap.getFieldContentsFromCerequirements(CeRequirements, SiteMap.CE_FIELD.NoUsers));

			if (matchRequest.get("oversubscribedJob") != null && lazyj.Utils.stringToBool(matchRequest.get("oversubscribedJob").toString(), false)) {
				ArrayList<String> usersOversubscription = getOversubscribedUsers(matchRequest.get("Users"), matchRequest.get("CE"), db);
				if (usersOversubscription == null) {
					logger.log(Level.INFO, "No jobs to execute in oversubscription regime");
					return matchAnswer;
				}
				matchRequest.put("Users", usersOversubscription);
			}
			if (matchRequest.get("Users") != null && !((ArrayList<String>) matchRequest.get("Users")).isEmpty()) {
				final ArrayList<String> users = (ArrayList<String>) matchRequest.get("Users");
				String orconcat = " and (";
				for (final String user : users) {
					final Integer userId = TaskQueueUtils.getUserId(user, true);

					if (userId != null) {
						where += orconcat + "userId = ?";
						orconcat = " or ";
						bindValues.add(userId);
					}
				}
				where += ")";
			}

			if (matchRequest.get("NoUsers") != null && !((ArrayList<String>) matchRequest.get("NoUsers")).isEmpty()) {
				final ArrayList<String> users = (ArrayList<String>) matchRequest.get("NoUsers");
				for (final String user : users) {
					final Integer userId = TaskQueueUtils.getUserId(user, true);

					if (userId != null) {
						where += " and userId != ? ";
						bindValues.add(userId);
					}
				}
			}

			if (matchRequest.containsKey("RequiredCpusCe")) {
				final Pattern pat = Pattern.compile("\\s*(>=|<=|>|<|==|=|!=)\\s*([0-9]+)");
				final Matcher m = pat.matcher((String) matchRequest.get("RequiredCpusCe"));
				if (m.matches()) {
					final String operator = "==".equals(m.group(1)) ? "=" : m.group(1);
					where += " and cpucores " + operator + " ? ";
					bindValues.add(Integer.valueOf(m.group(2)));
				}
			}

			// // Check matching CPU Architecture (x86_64 or aarch64 etc)
			if (matchRequest.containsKey("Platform")) {
				where += " and (platforms like concat('%,',?,',%') or platforms is null)";
				bindValues.add(TaskQueueUtils.extractPlatform((String) matchRequest.get("Platform")));
			}

			// Add Site Sonar Constraints
			// Initialize or refresh constraint cache
			HashMap<String, String> constraintCache = TaskQueueUtils.getConstraintCache();

			if (constraintCache != null && constraintCache.size() > 0) {
				// Constraint name is the constraint key
				for (Map.Entry<String, String> entry : constraintCache.entrySet()) {
					String constraintName = entry.getKey();
					String constraintType = entry.getValue();
					logger.log(Level.FINE, "Enforcing additional constraints for " + matchRequest.get("Localhost"));
					logger.log(Level.FINE, "Constraint name : " + constraintName);
					logger.log(Level.FINE, "Constraint expression : " + constraintType);

					// If the site map has a value for the constraint, add the constraint check
					if (matchRequest.containsKey(constraintName)) {
						Object constraintValue = matchRequest.get(constraintName);
						logger.log(Level.FINE, "Constraint value : " + constraintValue);
						if (constraintValue != null) {
							// By default, any node that either match the constraint value or does not have the constraint
							// values defined will be matched to the node. Only the jobs that specifically does not match to
							// the constraint value will be rejected
							if (constraintType.equals("equality")) {
								// eg:- SELECT * FROM JOB_AGENT WHERE... AND ((? = containsAVX) OR (containsAVX is null))
								// SELECT * FROM JOB_AGENT WHERE... ((1 = containsAVX) OR (containsAVX is null))
								where += " and (( ? = " + constraintName + ") or (" + constraintName + " is null))";
							}
							else if (constraintType.equals("regex")) {
								// SELECT * FROM JOB_AGENT WHERE... AND ((? LIKE CPU_FLAGS) OR (CPU_FLAGS is null))
								// SELECT * FROM JOB_AGENT WHERE... AND ((flag1 flag2 avx LIKE %avx%) OR (CPU_FLAGS is null))
								where += " and (( ? LIKE " + constraintName + ") or (" + constraintName + " is null))";
							}
							else {
								// stop matching because the constraint type is not supported
								logger.log(Level.SEVERE, "Incorrect expression type provided: " + constraintType);
								return matchAnswer;
							}
							// It is necessary to ensure constraint.jsp send the right data type and sitemap use the same data type
							// because the value comparison is done at runtime with type "Object"
							bindValues.add(constraintValue);
						}
					}
					else {
						logger.log(Level.FINE, "Site map does not contain a value for : " + constraintName +
								". Setting the constraint value to null");
						// Avoid accepting jobs that require the constraint value to be not present
						// eg:- SELECT * FROM JOB_AGENT WHERE... AND (containsAVX is null)
						// SELECT * FROM JOB_AGENT WHERE...(containsAVX is null)
						// Jobs that require containsAVX to be present will not be matched to this node
						where += " and (" + constraintName + " is null)";
					}
				}
			}

			Integer limit = Integer.valueOf(1);

			if (matchRequest.containsKey("limit"))
				limit = (Integer) matchRequest.get("limit");

			if (matchRequest.containsKey("OptimizeTTL")) {
				where += " and productionId is not NULL and TTLOptimizationType is not NULL";
			}

			if (isRemoteAccessAllowed) {
				logger.log(Level.INFO, "Checking for remote agents");

				// TODO: ask cache for ns:jobbroker key:remoteagents
				// $self->{CONFIG}->{CACHE_SERVICE_ADDRESS}?ns=jobbroker&key=remoteagents

				final String agents = getRemoteAgents();

				if (agents != null && !agents.isBlank()) {
					where += " and entryId in (" + agents + ")";

					// TODO: store in cache
					// $self->{CONFIG}->{CACHE_SERVICE_ADDRESS}?ns=jobbroker&key=remoteagents&timeout=300&value=".Dumper([@$agents])
				}
				else
					return matchAnswer;
			}

			db.setReadOnly(true);

			final String q = "select " + ret + " from JOBAGENT where priority>0 AND counter>0 " + where + " order by priority desc, price desc, oldestQueueId asc limit " + limit;

			if (logger.isLoggable(Level.FINE))
				logger.log(Level.FINE, "Going to select agents: " + DBUtils.applyBindParameters(q, bindValues));

			incrementMonitorCounter("TQ_db_lookup");

			if (!db.query(q, false, bindValues.toArray(new Object[0]))) {
				logger.log(Level.WARNING, "JobAgent selection query has failed", db.getLastError());
				return matchAnswer;
			}

			if (db.moveNext()) {
				final String[] columns = db.getColumnNames();
				matchAnswer.put("Code", Integer.valueOf(1));

				if (limit.intValue() > 1) {
					// Return all entries and check if one can be used (usually, for optimizations)
					final ArrayList<HashMap<String, Object>> entries = new ArrayList<>();
					do {
						HashMap<String, Object> entry = new HashMap<>();
						for (final String col : columns) {
							entry.put(col, db.gets(col));
						}
						logger.log(Level.INFO, "Putting " + entry);
						entries.add(entry);
					} while (db.moveNext());
					matchAnswer.put("entries", entries);
				}
				else {
					for (final String col : columns) {
						logger.log(Level.INFO, "Putting " + col + "-" + db.gets(col));
						matchAnswer.put(col, db.gets(col));
					}
				}
				matchAnswer.put("CE", matchRequest.get("CE"));
				matchAnswer.put("Host", matchRequest.get("Host"));

			}
			else if (logger.isLoggable(Level.FINE))
				logger.log(Level.FINE, "Nothing matches this request");

			return matchAnswer;
		}
	}

	private static ArrayList<String> getOversubscribedUsers(Object users, Object site, DBFunctions db) {
		@SuppressWarnings("unchecked")
		final ArrayList<String> siteUsers = (ArrayList<String>) users;
		if (site != null) {
			String q = "select users from OVERSUBSCRIBED_USERS where '" + site + "' like site_regex order by length(site_regex) desc limit 1";

			db.setReadOnly(true);
			db.query(q, false);

			if (db.moveNext()) {
				String allowedUsers = db.gets(1);
				if (allowedUsers.isEmpty()) { // There was an entry in the db for the site without any user, so it is disabled
					logger.log(Level.INFO, "Oversubscription disabled in db");
					return null;
				}
				logger.log(Level.INFO, "Got ovsersubscribed users " + allowedUsers);
				List<String> oversubscribedUsersList = new ArrayList<>();
				oversubscribedUsersList = Arrays.asList(allowedUsers.split(","));
				if (siteUsers.isEmpty()) {
					return new ArrayList<>(oversubscribedUsersList);
				}
				Set<String> intersection = siteUsers.stream().distinct().filter(oversubscribedUsersList::contains).collect(Collectors.toSet());
				if (intersection.isEmpty()) {
					logger.log(Level.INFO, "There were no matching users in the intersection. Can not run any job in oversubscription");
					return null;
				}
				return new ArrayList<>(intersection);
			}
		}
		// All users are enabled so we return original list
		return siteUsers;
	}

	/**
	 * @param host
	 * @param port
	 * @param ceName
	 * @param version
	 * @return a two-element list with the status code ([0]) and the number of
	 *         slots ([1])
	 */
	public static List<Integer> getNumberFreeSlots(final String host, final int port, final String ceName, final String version) {
		try (DBFunctions db = TaskQueueUtils.getQueueDB()) {
			if (db == null)
				return null;

			final ArrayList<Integer> code_and_slots = new ArrayList<>();
			code_and_slots.add(Integer.valueOf(0));

			final int hostId = TaskQueueUtils.getHostOrInsert(host, port, version);
			if (hostId == 0) {
				logger.severe("Error: getNumberFreeSlots, failed getHostOrInsert: " + host);
				code_and_slots.set(0, Integer.valueOf(1));
				return code_and_slots;
			}

			if (!TaskQueueUtils.updateHost(host, "ACTIVE", Integer.valueOf(1), port, version, ceName)) {
				logger.severe("Error: getNumberFreeSlots, failed updateHost: " + host);
				code_and_slots.set(0, Integer.valueOf(2));
				return code_and_slots;
			}

			if (!"".equals(ceName)) {
				final String blocking = TaskQueueUtils.getSiteQueueBlocked(ceName);

				if (blocking == null || !"open".equals(blocking)) {
					logger.info("The queue " + ceName + " is blocked in the master queue!");
					TaskQueueUtils.setSiteQueueStatus(ceName, "closed-blocked");
					code_and_slots.set(0, Integer.valueOf(-2));
					return code_and_slots;
				}
			}

			final ArrayList<Integer> slots = TaskQueueUtils.getNumberMaxAndQueuedCE(host, ceName);
			if (slots == null || slots.size() != 2) {
				logger.severe("Error: getNumberFreeSlots, failed to get slots: " + host + " - " + ceName);
				code_and_slots.set(0, Integer.valueOf(3));
				return code_and_slots;
			}

			code_and_slots.addAll(slots);

			return code_and_slots;
		}
	}

	/**
	 * @param matchRequest will be checked against LDAP if there's anything to add, if not already provided
	 */
	private static void updateWithValuesInLDAP(final HashMap<String, Object> matchRequest) {
		if (!matchRequest.containsKey("CheckedLDAP")) {
			if (matchRequest.containsKey("CE")) {
				Map<String, Object> CeConfig = null;
				if (matchRequest.containsKey("Site") && matchRequest.get("Site") != null) // careful, could contain key that's there but null
					CeConfig = ConfigUtils.getCEConfigFromLdap(false, matchRequest.get("Site").toString(), matchRequest.get("CE").toString().split("::")[2]);

				if (CeConfig != null && CeConfig.containsKey("ce_cerequirements")) {
					matchRequest.putIfAbsent("ce_requirements", CeConfig.get("ce_cerequirements").toString());
				}

				final String CeRequirements = Objects.isNull(matchRequest.get("ce_requirements")) ? "" : matchRequest.get("ce_requirements").toString();

				matchRequest.putIfAbsent("Users", SiteMap.getFieldContentsFromCerequirements(CeRequirements, SiteMap.CE_FIELD.Users));
				matchRequest.putIfAbsent("NoUsers", SiteMap.getFieldContentsFromCerequirements(CeRequirements, SiteMap.CE_FIELD.NoUsers));

				if (!matchRequest.containsKey("Partition")) {
					final String partitions = ConfigUtils.getPartitions((String) matchRequest.get("CE"));
					if (partitions != null && !",,".equals(partitions))
						matchRequest.put("Partition", partitions);
				}
			}
			matchRequest.put("CheckedLDAP", "");
		}
	}

}
