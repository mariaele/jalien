package alien.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import alien.config.ConfigUtils;
import alien.user.JAKeyStore;
import alien.user.UserFactory;

/**
 * @author Vova
 * @since May 2024
 */
@WebServlet("/jwk")
public class JWKServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String JAuthZPubLocation = ConfigUtils.getConfig().gets("jAuthZ.pub.key.location", UserFactory.getUserHome() +
				File.separator + ".alien" +
				File.separator + "authen" +
				File.separator + "lpub.pem");

		String jsonData = "{\"keys\":[{\n" +
				"    \"kty\": \"RSA\",\n" +
				"    \"use\": \"sig\",\n" +
				"    \"e\": \"AQAB\",\n" +
				"    \"kid\": \"ALICE\",\n" +
				"    \"n\": \"" +
				Base64.getUrlEncoder().encodeToString(Arrays.toString(Objects.requireNonNull(JAKeyStore.loadPubX509(JAuthZPubLocation, false))).getBytes()) + "\",\n" +
				"    \"alg\": \"RS256\"\n" +
				"}]}";

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try (PrintWriter w = response.getWriter()) {
			w.write(jsonData);
		}
	}
}
